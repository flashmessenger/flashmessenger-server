use axum::{
    http::StatusCode,
    response::IntoResponse,
    routing::{get, post},
    Json, Router,
};
use redis::RedisError;

use redis::Connection;
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use uuid::Uuid;

fn get_redis_connection() -> redis::RedisResult<Connection> {
    let client = redis::Client::open("redis://localhost:8300/")?;
    let con = client.get_connection()?;
    Ok(con) // from the south.
}

async fn root() -> Json<Value> {
    Json(json!({"flash": "messenger"}))
}

#[derive(Deserialize)]
struct CreateChannel {
    pub_key: String,
}

#[derive(Serialize)]
struct Channel {
    uuid: String,
    pub_key: String,
}

async fn create_channel(Json(payload): Json<CreateChannel>) -> impl IntoResponse {
    let uuid = Uuid::new_v4().to_string();

    let mut redis_con: Connection = match get_redis_connection() {
        Ok(con) => con,
        Err(_) => {
            return (
                StatusCode::SERVICE_UNAVAILABLE,
                "Failed to connect to Redis",
            )
                .into_response()
        }
    };
    let pub_key: String = payload.pub_key.to_string();
    // TODO: Check if this pubkey already exists in the database.

    let result: Result<redis::Value, RedisError> = redis::cmd("HSET")
        .arg(&uuid)
        .arg("pub_key")
        .arg(&pub_key)
        .query(&mut redis_con);

    match result {
        Ok(_) => (StatusCode::CREATED, Json(Channel { pub_key, uuid })).into_response(),
        Err(err) => (
            StatusCode::INTERNAL_SERVER_ERROR,
            Json(json!({"error": err.to_string()})),
        )
            .into_response(),
    }
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();

    let app = Router::new()
        .route("/", get(root))
        .route("/channels", post(create_channel));

    let listener = tokio::net::TcpListener::bind("[fd00::5:55f8]:8101")
        .await
        .unwrap();
    axum::serve(listener, app).await.unwrap();
}
