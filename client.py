import click
import json
import os


from cryptography.hazmat.primitives.asymmetric.ed25519 import Ed25519PrivateKey
from cryptography.hazmat.primitives.ciphers.aead import ChaCha20Poly1305


@click.group()
def cli():
    pass


@cli.command()
def generate_keys():
    """Generates the needed keys.

    - An authentication keypair, using Ed25519, which will be
      used to authenticate to the server;
    - An encryption key, using ChaCha20Poly1305.

    """
    auth_private_key = Ed25519PrivateKey.generate()
    encryption_key = ChaCha20Poly1305.generate_key()

    click.echo(
        json.dumps(
            {
                "auth_private_key": auth_private_key.private_bytes_raw().hex(),
                "encryption_key": encryption_key.hex(),
            }
        ),
    )


@cli.command()
@click.argument("name")
@click.argument("auth_private_key_hex")
@click.argument("encryption_key_hex")
@click.option("--domain", default="http://localhost:8000")
@click.option("--max_ttl", default=10, type=int)
def create_channel(name, auth_private_key_hex, encryption_key_hex, domain, max_ttl):
    click_message = (
        f'Creating a new channel named "{name}" on server {domain}, '
        f"with a max TTL of {max_ttl} seconds."
    )
    click.echo(click_message)

    # Sign[AuthPrivKey, (pubChannelInfo + Encrypt(EncryptPrivKey, data))]
    auth_private_key_bytes = bytes.fromhex(auth_private_key_hex)
    auth_private_key = Ed25519PrivateKey.from_private_bytes(auth_private_key_bytes)
    public_key_bytes = auth_private_key.public_key().public_bytes_raw()

    chacha = ChaCha20Poly1305(bytes.fromhex(encryption_key_hex))
    nonce = os.urandom(12)

    data = json.dumps(dict(name=name))
    channel_payload = chacha.encrypt(nonce, data.encode("utf-8"), public_key_bytes)

    signature = auth_private_key.sign(channel_payload)
    click.echo(f"payload: {channel_payload.hex()}", color=True)
    click.echo(f"signature: {signature.hex()}", color=True)


if __name__ == "__main__":
    cli()
