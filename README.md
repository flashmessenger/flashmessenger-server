# Flash Messenger Server

> [!IMPORTANT]
> This is all a work in progress. Do not use, it's mostly here for fun.

To set it up, with a rust installed on your system:

```bash
rust up
cargo run
```

## To compile the server

On a mac m1 machine, to cross compile:

```bash
# Install the dependencies for the target
rustup target x86_64-unknown-linux-gnu

# Install the linker and tools for x86_64 linux
brew tap SergioBenitez/osxct
brew install sergiobenitez/osxct/x86_64-unknown-linux-gnu

# Compile for this target
cargo build --target x86_64-unknown-linux-gnu
````

## Authentication flow

```mermaid
sequenceDiagram
    participant Client
    participant Server

    Note over Client: Generates AuthenticationKeypair (AuthPubKey, AuthPrivKey)
    Note over Client: Generates Encryption key

    Client->>Server: HTTP POST /channels
    Note over Client: Sign[AuthPrivKey, (pubChannelInfo + Encrypt(encryptionKey, data))]


    Server->>Server: Verify signature with AuthPubKey
    Server->>Server: Check that AuthPubKey is unique
    Server-->>Client: Returns UUID
    Note over Server: Stores publicChannelInfo
    Note over Server: Stores encrypted data

    Client->>Server: HTTP POST /messages
    Note over Client: Sign[AuthPrivKey, (pubMessage + Encrypt(encryptionKey, privMessage))]
    Server->>Server: Verify signature
    Server->>Server: Get UUID from AuthPubKey
    Server->>Server: Update channel with new message
    Server-->>Client: Acknowledgment of message addition
```

For channel creation:

- `pubChannelInfo` = `AuthPubKey` + `maxTTL`
- `data` = `name` + `logo` + metadata

For sending messages:

- `pubMessage` = TTL (time to leave for the message)
- `privMessage` = `blob` + metadata


## Usage

There is a tiny python client library for now, that make it possible to interact with the server. You can install it with:

```bash
# Create a virtual environment to avoid polluting the system python
python -m venv venv
# Install the python dependencies
venv/bin/pip install -r requirements.txt
```

Then to use it

```bash
# Generate the keypair. It doesn't store anything, and return JSON.
$ venv/bin/python client.py generate-keys | jq
{
  "auth_private_key": "de812fb3023034e526327dcd841cb77d2a0235ee09ee3ec5caedae7ce86d794a",
  "encryption_key": "605d33eb3b40877b7e4181e2400c08611e9d5004ab7a50f5e091dfa23ac855aa"
}

# Create a channel
$ venv/bin/python client.py create-channel yeah de812fb3023034e526327dcd841cb77d2a0235ee09ee3ec5caedae7ce86d794a 605d33eb3b40877b7e4181e2400c08611e9d5004ab7a50f5e091dfa23ac855aa
Creating a new channel named "yeah" on server http://localhost:8000, with a max TTL of 10 seconds.
payload: b34981bdbba9845414e7626022bb6190f0a4095550411663ad33a1f1586f4baa
signature: abb64ba162ff5593a79eeedfe2e9f23941a7f5eb091d7a4611c467fa4013332da5c284e5445f5065d0c2cf76a2d109197e28a806a873af3502c89cd656b6900f
```